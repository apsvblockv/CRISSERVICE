package es.upm.dit.apsv.cris.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

//Esto forma parte de la ruta de entrada, cuando llegue una petición que contenga este /rest se vendrá a los servicios que tengamos aquí
@ApplicationPath("rest")
public class CRISApp extends ResourceConfig {
	public CRISApp() {
		packages("es.upm.dit.apsv.cris.rest");
	}

}
